parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}


program: TK_class LCURLY field_decl* method_decl* RCURLY EOF;

field_decl: field_new (VIRGULA field_new)* PONTOEVIRGULA;

field_new: escopo_local| escopo_local LCOISA DIGITS RCOISA;

method_decl: type IDENTIFICADOR LPARA (escopo_local (VIRGULA escopo_local)*)* RPARA block;

escopo_local: type? IDENTIFICADOR;

block: LCURLY var_decl* statement* RCURLY; 

var_decl: escopo_local (VIRGULA escopo_local)* PONTOEVIRGULA;

//var_decl: INT (VIRGULA escopo_local)* PONTOEVIRGULA;

type: INT | BOO | VOID;

statement: location assign_op expr PONTOEVIRGULA| method_call PONTOEVIRGULA | IF LPARA expr RPARA block (ELSE block)* | FOR id IGUAL expr VIRGULA expr block | RETURN expr? PONTOEVIRGULA | BREAK PONTOEVIRGULA | CONTINUE PONTOEVIRGULA| block;

assign_op: IGUAL | MAISIUGUAL | MENOSIGUAL;

method_call: method_name LPARA (expr (VIRGULA expr)*)* RPARA | CALLOUT LPARA string_literal (VIRGULA callout_arg)* RPARA;

method_name: IDENTIFICADOR ;

location: IDENTIFICADOR | IDENTIFICADOR LCOISA expr RCOISA ;

expr: location | method_call | literal | expr bin_op expr | SUBTRACAO expr | EXCLAN expr | LPARA expr* RPARA;

callout_arg: expr | string_literal ;

bin_op: ( arith_op | rel_op | eq_op | cond_op );

arith_op: (SOMA | SUBTRACAO | DIVISAO | MULTIPLICAR | MODULO);

rel_op: (MAIOR | MENOR | MAIORIGUAL | MENORIGUAL);

eq_op: (COMPARACAO | DIFERENTE);

cond_op: (ECOMERCIAL | OULOGICO);

literal: int_literal | char_literal | bool_literal;

id: IDENTIFICADOR;

digit: DIGITS;

int_literal: decimal_literal;

decimal_literal: digit | HEXADECI ;

bool_literal: TRUE | FALSE;

char_literal: CHAR;

string_literal: STRING;







